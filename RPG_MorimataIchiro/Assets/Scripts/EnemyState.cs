﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyState : MonoBehaviour 
{
	public int dest = 0;//destinationpoint

	public Vector3 startPos;

	public float walkSpeed;

	private GameObject target;

	public GameObject patrolpoint;

	[SerializeField]
	UnityEngine.AI.NavMeshAgent	enemyAgent;

	[SerializeField]
	public Animator gobanimator;

	public enum enemyStates
	{
		Idle,
		Patrol,
		Chase,
		Attack,
	}
	//Idle , Patrol, Chase, Attack, Current

	public enemyStates TheState;

	// Use this for initialization
	void Start () 
	{
		gobanimator = this.GetComponent<Animator> ();
		enemyAgent = this.GetComponent<UnityEngine.AI.NavMeshAgent> ();
		enemyAgent.speed = walkSpeed;
		startPos = this.transform.position;
		TheState = enemyStates.Idle;
		gobanimator.SetTrigger ("Idle");
		patrolpoint = GameObject.Find ("Patrol point");
	}

	void Update()
	{
		
		switch (TheState) 
		{
		case enemyStates.Idle:
			Idle_State ();
			break;
		case enemyStates.Patrol:
			Patrol_State ();
			break;

		case enemyStates.Chase:
			Chase_State	();
			break;

		case enemyStates.Attack:
			Attack_State ();
			break;
		
		default:
			break;
		}
	}

    public void ToChaseState (GameObject target)
    {
        this.TheState = enemyStates.Chase;
        this.target = target;
       
    }

    public void ToPatrolState(GameObject target)
    {
        this.TheState = enemyStates.Patrol;
    }

	public void ToAttackState(GameObject target)
	{
		this.TheState = enemyStates.Attack;
	}

	public void Idle_State()
	{
		gobanimator.SetTrigger ("Idle");
        Debug.Log("Idle");
	}

	public void Patrol_State()
	{
		gobanimator.ResetTrigger ("Idle");
		enemyAgent.speed = walkSpeed;
		gobanimator.SetTrigger ("Walk");
		route ();
        Debug.Log("Patrol");
	}

	public void route()
	{
		switch (dest) 
		{
		case 0:
			enemyAgent.destination = patrolpoint.transform.position;
			if (enemyAgent.remainingDistance < 0.0001f)
			{
				dest = 1;
			}
			break;
		case 1:
			enemyAgent.destination = startPos;
			if (enemyAgent.remainingDistance < 0.0001f)
			{
				dest = 0;
			}
			break;
		}

	}

	public void Chase_State()
	{
		gobanimator.ResetTrigger ("Idle");
		enemyAgent.speed = walkSpeed * 3;
		this.transform.LookAt (GameObject.FindGameObjectWithTag("Player").transform, Vector3.up);
		enemyAgent.destination = GameObject.FindGameObjectWithTag ("Player").transform.position;
		gobanimator.SetTrigger ("Run");
        Debug.Log("Chase");
	}

	public void Attack_State()
	{
		gobanimator.ResetTrigger ("Idle");
		gobanimator.ResetTrigger ("Run");
		gobanimator.ResetTrigger ("Walk");
		this.transform.LookAt (GameObject.FindGameObjectWithTag("Player").transform, Vector3.up);
		gobanimator.SetTrigger ("Attack");
        Debug.Log("Attack");
	}

}
