﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Enemystats : MonoBehaviour {

	public GameObject[] drops;

	float timer;
	bool dead;
	bool playerdead;

	public float enemyCurrentHP;
	public float enemyTotalHP;
	public int enemyAttack;
	public int enemyrVit;
	public int enemyStr;

	public Image HPBAR;


	// Use this for initialization
	void Awake () {
		float enemyCurrentHP = 100;
		float enemyTotalHP = 100;
		int enemyAttack = 5;
	}
	
	// Update is called once per frame
	void Update () {
		HPBAR.fillAmount = enemyCurrentHP / enemyTotalHP;
		HPBAR.transform.LookAt (Camera.main.transform);
		if (enemyCurrentHP <= 0) 
		{
			GameObject.FindGameObjectWithTag ("Player").GetComponent<PlayerBehavior> ().kokoAnimation.ResetTrigger ("KK_Attack");
			GameObject.FindGameObjectWithTag ("Player").GetComponent<PlayerBehavior> ().kokoAnimation.SetTrigger ("KK_Idle");
			GameObject.FindGameObjectWithTag ("Player").GetComponent<PlayerBehavior> ().target = null;
			this.GetComponent<EnemyState> ().gobanimator.ResetTrigger ("Idle");
			this.GetComponent<EnemyState> ().gobanimator.ResetTrigger ("Run");
			this.GetComponent<EnemyState> ().gobanimator.ResetTrigger ("Walk");
			this.GetComponent<EnemyState> ().gobanimator.ResetTrigger ("Attack");
			dead = true;
		}

		if (dead) 
		{
			timer += Time.deltaTime;
			if (timer >= 3) 
			{
				int roll;
				roll = Random.Range (0, 100);
				if (roll >= 70) 
				{
					Instantiate (drops[0], transform.position, Quaternion.identity);
				}
				else if (roll >= 80) 
				{
					Instantiate (drops[0], transform.position, Quaternion.identity);
					Instantiate (drops[1], transform.position, Quaternion.identity);
				}
				Destroy (this.gameObject);
				GameObject.Find ("Stats").GetComponent<PlayerStats> ().SendMessage ("EXP");
			}
		}

		if (playerdead) 
		{
			timer += Time.deltaTime;
			if (timer >= 5) 
			{
				Destroy (GameObject.FindGameObjectWithTag ("PlayerData"));
				SceneManager.LoadScene (0);
			}
		}
	}

	void Health(float dmg)
	{
		enemyCurrentHP -= dmg;
	}

	public void DamageTo()
	{
		//Debug.Log ("THIS WORKS!!!");	
		GameObject.FindGameObjectWithTag ("Player").GetComponent<PlayerBehavior> ().SendMessage("Health", enemyAttack);
		if (GameObject.FindGameObjectWithTag("PlayerData").GetComponent<PlayerData>().playerCurrentHP <= 0) 
		{
			GameObject.FindGameObjectWithTag ("Player").GetComponent<PlayerBehavior> ().kokoAnimation.ResetTrigger("KK_Idle");
			GameObject.FindGameObjectWithTag ("Player").GetComponent<PlayerBehavior> ().kokoAnimation.ResetTrigger("KK_Run_No");
			GameObject.FindGameObjectWithTag ("Player").GetComponent<PlayerBehavior> ().kokoAnimation.ResetTrigger("KK_Attack");
			GameObject.FindGameObjectWithTag ("Player").GetComponent<PlayerBehavior> ().kokoAnimation.SetTrigger("KK_Dead");
			GameObject.FindGameObjectWithTag ("Player").GetComponent<PlayerBehavior> ().enabled = false;
			playerdead = true;
		}

	}

		
}
