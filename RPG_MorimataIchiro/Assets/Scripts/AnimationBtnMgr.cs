﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class AnimationBtnMgr : MonoBehaviour {

	public GameObject playerButtons; 	
	public GameObject monsterButtons; 	
	public GameObject npcButtons; 	
	public GameObject[] cameraPos;
	public GameObject cameraMain;

	public GameObject Koko;
	public GameObject npc;
	public GameObject[] Monster;

	private Animator kokoAnimation;
	private Animator npcAnimation;
	public Animator[] monsterAnimation;



	void Start()
	{
		kokoAnimation 	= Koko.GetComponent<Animator> ();
		npcAnimation 	= npc.GetComponent<Animator> ();

		monsterAnimation[0] = Monster[0].GetComponent<Animator> ();
		monsterAnimation[1] = Monster[1].GetComponent<Animator> ();
		monsterAnimation[2] = Monster[2].GetComponent<Animator> ();


		playerButtons.SetActive(true);
		npcButtons.SetActive (false);
		monsterButtons.SetActive (false);
		cameraMain.transform.position = cameraPos [0].transform.position;
	}


	//Player

	public void Player()
	{
		playerButtons.SetActive(true);
		npcButtons.SetActive (false);
		monsterButtons.SetActive (false);
		cameraMain.transform.position = cameraPos [0].transform.position;
	}

	public void KK_Idle()
	{
		kokoAnimation.SetTrigger("KK_Idle");
	}

	public void KK_Run()
	{
		kokoAnimation.SetTrigger("KK_Run_No");
	}

	public void KK_Atk_Run()
	{
		kokoAnimation.SetTrigger("KK_Run");
	}

	public void KK_Atk()
	{
		kokoAnimation.SetTrigger("KK_Attack");
	}

	public void KK_Attack_Standby()
	{
		kokoAnimation.SetTrigger("KK_Attack_Standby");
	}

	public void KK_Combo()
	{
		kokoAnimation.SetTrigger("KK_Combo");
	}

	public void KK_DrawBlade()
	{
		kokoAnimation.SetTrigger("KK_DrawBlade");
	}

	public void KK_PutBlade()
	{
		kokoAnimation.SetTrigger("KK_PutBlade");
	}

	public void KK_Damage()
	{
		kokoAnimation.SetTrigger("KK_Damage");
	}

	public void KK_Skill()
	{
		kokoAnimation.SetTrigger("KK_Skill");
	}

	public void KK_Dead()
	{
		kokoAnimation.SetTrigger("KK_Dead");
	}

	/// NPC

	public void NPCBtn()
	{
		playerButtons.SetActive(false);
		npcButtons.SetActive (true);
		monsterButtons.SetActive (false);
		cameraMain.transform.position = cameraPos [2].transform.position;
	}

	public void NPC_Idle()
	{
		npcAnimation.SetTrigger("NPC_Idle");
	}

	public void NPC_Talk()
	{
		npcAnimation.SetTrigger("NPC_Talk");
	}
	// Monster

	public void MonsterBtn()
	{
		playerButtons.SetActive(false);
		npcButtons.SetActive (false);
		monsterButtons.SetActive (true);
		cameraMain.transform.position = cameraPos [1].transform.position;
	}

	public void Idle ()
	{
		monsterAnimation [0].SetTrigger ("Idle");
		monsterAnimation [1].SetTrigger ("Idle");
		monsterAnimation [2].SetTrigger ("Idle");
	}

	public void Walk ()
	{
		monsterAnimation [0].SetTrigger("Walk");
		monsterAnimation [1].SetTrigger("Walk");
		monsterAnimation [2].SetTrigger("Walk");
	}

	public void Run ()
	{
		monsterAnimation[0].SetTrigger("Run");
		monsterAnimation[1].SetTrigger("Run");
		monsterAnimation[2].SetTrigger("Run");
	}

	public void Attack ()
	{
		monsterAnimation[0].SetTrigger("Attack");
		monsterAnimation[1].SetTrigger("Attack");
		monsterAnimation[2].SetTrigger("Attack");
	}

	public void Damage ()
	{
		monsterAnimation[0].SetTrigger("Damage");
		monsterAnimation[1].SetTrigger("Damage");
		monsterAnimation[2].SetTrigger("Damage");
	}

	public void Skill ()
	{
		monsterAnimation[0].SetTrigger("Skill");
		monsterAnimation[1].SetTrigger("Skill");
		monsterAnimation[2].SetTrigger("Skill");
	}

	public void Dead ()
	{
		monsterAnimation[0].SetTrigger("Dead");
		monsterAnimation[1].SetTrigger("Dead");
		monsterAnimation[2].SetTrigger("Dead");
	}

	public void ReturnToMain()
	{
		SceneManager.LoadScene("Main");
	}


}
