﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class SceneMgr : MonoBehaviour {

	//Put scenes on the build first, then pick the scene number(0 ~ n)
	public int scenenumber;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void SceneInGame()
	{
		SceneManager.LoadScene("2.Village");
	}


	public void SceneAnimation()
	{
		SceneManager.LoadScene("Animations Scene");
	}

	void OnTriggerEnter(Collider col)
	{
		if (col.gameObject.CompareTag("Player"))
		{
			SceneManager.LoadScene (scenenumber);
		}
	}
}
