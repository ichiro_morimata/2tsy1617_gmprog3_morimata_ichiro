﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class PlayerStats : MonoBehaviour 
{
	PlayerData playerdata;

	public string playerName;
	public int playerLevel;
	public float playerCurrentHP;
	public float playerTotalHP;
	public float playerCurrentMP;
	public float playerTotalMP;
	public int playerAttack;
	public int playerMAttack;
	public int playerVit;
	public int playerStr;
	public int playerSpr;
	public int playerInt;
	public float playerCurrentExp;
	public float playerTotalExp;
	public float playerExptoLevel;
	public int playerStatpts;

	public Image expGained;
	public Image HPBAR;
	public Image MPBAR;

	public GameObject[] button;
	public Text[] StatUI;
	bool Levelup = false;
	//money += 5;
	//strenght.text = "Str: " + money.ToString ();


	void Awake()//initiates before Start
	{
		playerdata = GameObject.FindGameObjectWithTag ("PlayerData").GetComponent<PlayerData> ();
		expGained = GameObject.Find ("EXPBAR").GetComponent<Image>();
		HPBAR = GameObject.Find ("HPBar").GetComponent<Image>();
		MPBAR = GameObject.Find ("MPBar").GetComponent<Image>();
	}

	// Use this for initialization
	void Start () 
	{
		

	}

	public void Vit()
	{
		playerdata.playerStatpts 	-= 1;
		playerdata.playerVit 		+= 1;
		playerdata.playerTotalHP 	= playerdata.playerVit * 5;
	}

	public void Str()
	{
		playerdata.playerStatpts 	-= 1;
		playerdata.playerStr 		+= 1;
		playerdata.playerAttack 	= playerStr * 2;
	}

	public void Spr()
	{
		playerdata.playerStatpts 	-= 1;
		playerdata.playerSpr 		+= 1;
		playerdata.playerTotalMP 	= playerdata.playerSpr * 5;
	}

	public void Int()
	{
		playerdata.playerStatpts 	-= 1;
		playerdata.playerInt 		+= 1;
		playerdata.playerMAttack 	= playerInt * 2;
	}

	public void EXP()
	{
		playerdata.playerCurrentExp += 100;
	}

	// Update is called once per frame
	void Update () 
	{
		playerName 		= playerdata.playerName;
		playerLevel 	= playerdata.playerLevel;
		playerTotalHP 	= playerdata.playerTotalHP;
		playerCurrentHP	= playerdata.playerCurrentHP;
		playerAttack 	= playerdata.playerAttack;
		playerVit 		= playerdata.playerVit;
		playerStr 		= playerdata.playerStr;
		playerCurrentExp= playerdata.playerCurrentExp;
		playerStatpts 	= playerdata.playerStatpts;
		playerCurrentMP = playerdata.playerCurrentMP;
		playerTotalMP  	= playerdata.playerTotalMP;
		playerMAttack  	= playerdata.playerMAttack;
		playerSpr		= playerdata.playerSpr;
		playerInt		= playerdata.playerInt;

		playerExptoLevel = playerdata.playerExptoLevel;

		HPBAR.fillAmount = (playerdata.playerCurrentHP / playerdata.playerTotalHP);
		MPBAR.fillAmount = (playerdata.playerCurrentMP / playerdata.playerTotalMP);
		expGained.fillAmount = ((playerdata.playerCurrentExp / playerdata.playerExptoLevel));
		// Debug.Log ((playerCurrentExp / playerTotalExp) + " : " + playerCurrentExp + " : " + playerTotalExp);
		button[0].gameObject.SetActive(false);
		button[1].gameObject.SetActive(false);

		StatUI [0].text = "Name : " + playerdata.playerName.ToString ();
		StatUI [1].text = "Level : " + playerdata.playerLevel.ToString ();
		StatUI [2].text = "Health : " /*+ playerdata.playerCurrentHP.ToString () +  " / "*/ + playerdata.playerTotalHP.ToString();
		StatUI [3].text = "Attack : " + playerdata.playerAttack.ToString ();
		StatUI [4].text = "Vit : " + playerdata.playerVit.ToString ();
		StatUI [5].text = "Str : " + playerdata.playerStr.ToString ();
		StatUI [6].text = "Exp : " + playerdata.playerCurrentExp.ToString () +	 " / "+ playerdata.playerExptoLevel.ToString ();
		StatUI [7].text = "Stat Points : " + playerdata.playerStatpts.ToString ();
		StatUI [8].text = "Mana : " /*+ playerdata.playerCurrentMP.ToString () + "/" */+ playerdata.playerTotalMP.ToString ();
		StatUI [9].text = "Int : " + playerdata.playerInt.ToString ();
		StatUI [10].text = "Spr : " + playerdata.playerSpr.ToString ();
		StatUI [11].text = "MAttack :" + playerdata.playerMAttack.ToString ();

		if (playerCurrentHP > playerTotalHP) 
		{
			playerdata.playerCurrentHP = playerdata.playerTotalHP;
		}

		if (playerCurrentMP > playerTotalMP) 
		{
			playerdata.playerCurrentMP = playerdata.playerTotalMP;
		}

		if (playerStatpts > 0) 
		{
			button [0].SetActive(true);
			button [1].SetActive(true);
			button[3].gameObject.SetActive(true);
			button[4].gameObject.SetActive(true);
		}

		if (playerdata.playerStatpts <1) 
		{
			button[0].gameObject.SetActive(false);
			button[1].gameObject.SetActive(false);
			button[3].gameObject.SetActive(false);
			button[4].gameObject.SetActive(false);
		}


		if (playerdata.playerCurrentExp >= playerdata.playerExptoLevel) 
		{
			playerdata.playerLevel 		+= 1;
			playerdata.playerCurrentExp	-= playerdata.playerExptoLevel;

			playerdata.playerStatpts 		+= 3;
			Levelup = true;
		}

		if (Levelup) {
			playerdata.playerExptoLevel 	+= 100;
			Levelup = false;
		}
			
	}


}
