﻿using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PlayerBehavior : MonoBehaviour 
{
	UnityEngine.AI.NavMeshAgent	kokoAgent;

	float timer;

	public GameObject		markerPoint;
	public Animator 		kokoAnimation;
	public GameObject[]	 	userInterface;

	public bool 			wayPoint;

	public Transform 		target;
    public Transform 	    player;

	private int hitRange = 1;

    private float inputHoldDelay = 0.5f;
    private WaitForSeconds inputHoldWait;
    private Vector3 destinationPos;


	// Use this for initialization
	void Start () 
	{
		kokoAgent 		= GetComponent<UnityEngine.AI.NavMeshAgent> ();
		kokoAnimation 	= GetComponent<Animator> ();

		kokoAnimation.SetTrigger("KK_Idle");

        kokoAgent.updateRotation = false;

        inputHoldWait = new WaitForSeconds(inputHoldDelay);

        destinationPos = transform.position;
	}
	
	// Update is called once per frame
	void Update ()
	{
		userInterface [1] = GameObject.FindGameObjectWithTag ("Inventory");

		if (EventSystem.current.IsPointerOverGameObject ())
		return;
		
		if (Input.GetMouseButtonDown (0)) {
			playerMove ();
		}

		if (target != null) {
			this.transform.LookAt (target, Vector3.up);
			kokoAgent.destination = target.position;
		}
			
		playerUI ();
	}

	void Health(float dmg)
	{
		GameObject.FindGameObjectWithTag("PlayerData").GetComponent<PlayerData>().playerCurrentHP -= dmg;
	}

	public void Attack()
	{
		kokoAnimation.ResetTrigger("KK_Idle");
		kokoAnimation.ResetTrigger("KK_Run_No");
		kokoAnimation.SetTrigger("KK_Attack");
		this.transform.LookAt (target, Vector3.up);
		if (target == null) 
		{
			kokoAnimation.SetTrigger("KK_Idle");
			kokoAnimation.ResetTrigger("KK_Run_No");
			kokoAnimation.ResetTrigger("KK_Attack");
		}
	}

	public void DamageTo()
	{
		//Debug.Log ("THIS WORKS!!!");	
		target.GetComponent<Enemystats>().SendMessage("Health", GameObject.FindGameObjectWithTag("PlayerData").GetComponent<PlayerData>().playerAttack);
		target.GetComponent<EnemyState> ().gobanimator.SetTrigger ("Damage");
		if (target.GetComponent<Enemystats> ().enemyCurrentHP <= 0) 
		{
			target.GetComponent<EnemyState> ().gobanimator.ResetTrigger ("Idle");
			target.GetComponent<EnemyState> ().gobanimator.ResetTrigger ("Run");
			target.GetComponent<EnemyState> ().gobanimator.ResetTrigger ("Walk");
			target.GetComponent<EnemyState> ().gobanimator.ResetTrigger ("Attack");
			target.GetComponent<EnemyState> ().gobanimator.SetTrigger ("Dead");
		}
	}

	public void playerMove()
	{
		RaycastHit hit;


		if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hit, 100)) 
		{
			if (hit.collider.tag != "Enemy") {
				Debug.Log ("not enemy");
				target = null;
				wayPoint = false;
				kokoAgent.destination = hit.point;

				//kokoAnimation.SetInteger ("KoKo Index", 2);
				kokoAnimation.SetTrigger ("KK_Run_No");

				if (wayPoint == false) {
					Destroy (GameObject.FindGameObjectWithTag ("Marker"));
					Instantiate (markerPoint, hit.point, Quaternion.identity);
					wayPoint = true;
				}

				this.transform.LookAt (GameObject.FindGameObjectWithTag ("Marker").transform, Vector3.up);
			}

			else 
			{
				target = hit.transform;
				Destroy (GameObject.FindGameObjectWithTag ("Marker"));
				wayPoint = false;
				this.transform.LookAt (target, Vector3.up);
				//this.transform.LookAt (hit.transform.position, Vector3.up);
				//kokoAgent.destination = hit.transform.position;
				kokoAgent.destination = target.position;
				kokoAnimation.SetTrigger ("KK_Run_No");
			}
		}

		float dist;
		dist = Vector3.Distance(target.transform.position, this.transform.position);
		if (dist < 3)
		{
			Attack ();
		}
	}

	public void playerUI()
	{

		// For active and inactive
		if (Input.GetKeyDown (KeyCode.C) && userInterface[0].GetComponent<Canvas>().enabled == false) 
		{
			userInterface [0].GetComponent<Canvas>().enabled = true;
		}

		else if (Input.GetKeyDown (KeyCode.C) && userInterface[0].GetComponent<Canvas>().enabled == true) 
		{
			userInterface [0].GetComponent<Canvas>().enabled = false;
		}


		// For active and inactive
		if (Input.GetKeyDown (KeyCode.I) && userInterface[1].GetComponent<Canvas>().enabled == false) 
		{
			userInterface [1].GetComponent<Canvas>().enabled = true;
		}

		else if (Input.GetKeyDown (KeyCode.I) && userInterface[1].GetComponent<Canvas>().enabled == true) 
		{
			userInterface [1].GetComponent<Canvas>().enabled = false;
		}
	}
	void OnTriggerEnter(Collider KokoCol)
	{

		wayPoint = false;

		if (KokoCol.gameObject.CompareTag("Marker"))
		{
			Destroy (KokoCol.gameObject);
			kokoAnimation.SetTrigger("KK_Idle");
		}

		if(KokoCol.gameObject.CompareTag("Enemy"))
		{
			if(target != null)
			Attack ();
		}

	}
}
