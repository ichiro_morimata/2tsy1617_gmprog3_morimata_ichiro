﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.UI;

[Serializable]
public class Questlist
{
	public string Questname;
	public string Questdescription;
	public string target;
	public int currentkill;
	public int targetkill;
	public int Goldreward;
	public int Expreward;
	public Button Cancel;
}
