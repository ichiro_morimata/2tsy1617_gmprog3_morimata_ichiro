﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shop : MonoBehaviour {

	public GameObject Shopinterface;
	public bool shopinrange;
	public Inventory itemlistings;

	// Use this for initialization
	void Start () 
	{
		
	}
	
	// Update is called once per frame
	void Update () 
	{
		itemlistings = GameObject.FindGameObjectWithTag ("Inventory").GetComponent<Inventory> ();

		if (shopinrange) 
		{
			if (Input.GetKeyDown (KeyCode.E) && Shopinterface.GetComponent<Canvas> ().enabled == false) 
			{
				Shopinterface.GetComponent<Canvas> ().enabled = true;
			} 

			else if (Input.GetKeyDown (KeyCode.E) && Shopinterface.GetComponent<Canvas> ().enabled == true) 
			{
				Shopinterface.GetComponent<Canvas> ().enabled = false;
			}
		}
	}

	public void buy(int Goldprice)
	{
		if (GameObject.FindGameObjectWithTag ("PlayerData").GetComponent<PlayerData> ().Gold >= Goldprice) 
		{
			GameObject.FindGameObjectWithTag ("PlayerData").GetComponent<PlayerData> ().Gold -= Goldprice;
			itemlistings.Additem ((Goldprice / 30) - 1);
		}
	}

	void OnTriggerEnter(Collider col)
	{
		if (col.transform.CompareTag ("Player"))
		{
			shopinrange = true;
		}
	}

	void OnTriggerExit(Collider col)
	{
		if (col.transform.CompareTag ("Player"))
		{
			Shopinterface.GetComponent<Canvas> ().enabled = false;
			shopinrange = false;
		}
	}
}
