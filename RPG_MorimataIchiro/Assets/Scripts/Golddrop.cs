﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Golddrop : MonoBehaviour {

	public int Goldamount;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnTriggerEnter(Collider col)
	{
		if(col.transform.CompareTag("Player"))
		{
			GameObject.FindGameObjectWithTag ("PlayerData").GetComponent<PlayerData>().Gold += (Goldamount * Random.Range(1,5));
			Destroy (this.gameObject);
		}
	}
}
