﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using UnityEngine.Serialization;

[Serializable]
public class Itemtype 
{	
	public string Name;
	public string Description;
	public int stock;
	public float heal;
	public int buff;
	public Text Descriptionbox;
	public Text stockcount;

	Itemtype()
	{
	}


}
