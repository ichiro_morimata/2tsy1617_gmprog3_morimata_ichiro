﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Itemdrop : MonoBehaviour {
	public int itemtype;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnTriggerEnter(Collider col)
	{
		if(col.transform.CompareTag("Player"))
		{
			GameObject.FindGameObjectWithTag ("Inventory").GetComponent<Inventory> ().Additem (itemtype);
			Destroy (this.gameObject);
		}
	}
}
