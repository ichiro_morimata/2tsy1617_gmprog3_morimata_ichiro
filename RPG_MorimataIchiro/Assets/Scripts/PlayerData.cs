﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerData : MonoBehaviour 
{
	public static PlayerData instance;
	public int Gold; 
	public Text Goldtext;
	public string playerName;
	public int playerLevel;
	public float playerCurrentHP;
	public float playerTotalHP;
	public float playerCurrentMP;
	public float playerTotalMP;
	public int playerAttack;
	public int playerMAttack;
	public int playerVit;
	public int playerStr;
	public int playerSpr;
	public int playerInt;
	public float playerCurrentExp;
	public float playerTotalExp;
	public float playerExptoLevel;
	public int playerStatpts;

	void Awake()
	{
		if (instance == null) 
		{
			Gold = 0;
			playerName 		= "KoKo";
			playerLevel 	= 1;
			playerVit 		= 20;
			playerStr 		= 10;
			playerSpr		= 5;
			playerInt		= 5;
			playerTotalHP 	= playerVit * 5;
			playerCurrentHP	= playerVit * 5;
			playerTotalMP 	= playerSpr * 5;
			playerCurrentMP	= playerSpr * 5;
			playerAttack 	= playerStr * 2;
			playerMAttack 	= playerInt * 2; 
			playerCurrentExp= 0;
			playerStatpts 	= 5;

			playerExptoLevel = 100;

			instance = this;
			DontDestroyOnLoad (this);
		}

		else if (this != instance) 
		{
			Destroy (this.gameObject);
		}
	}

	void Update()
	{
		Goldtext.text = "" + Gold;
		if(playerCurrentMP < playerTotalMP)
		playerCurrentMP += playerSpr * 0.05f * Time.deltaTime;
	}
}
