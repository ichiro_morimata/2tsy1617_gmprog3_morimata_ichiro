﻿using UnityEngine;
using System.Collections;

public class CameraBehavior : MonoBehaviour {

	public GameObject cameraTarget;

	private Vector3 offset;

	//public int targetIndex = 0;

	// Use this for initialization
	void Start () {

		offset = transform.position - cameraTarget.transform.position;

		transform.position = cameraTarget.transform.position + offset;
	}
	
	// Update is called once per frame
	void Update () {

			transform.position = cameraTarget.transform.position + offset;

	}
}
