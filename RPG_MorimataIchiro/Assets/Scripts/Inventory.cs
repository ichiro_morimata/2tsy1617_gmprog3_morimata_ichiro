﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Inventory : MonoBehaviour {

	public List<Itemtype> Inventorylist = new List<Itemtype>();
	public List<Itemtype> Bag = new List<Itemtype> ();

	public float bufftime;
	public bool tempbuffstr;
	public bool tempbuffvit;
	public bool tempbuffspr;
	public bool tempbuffint;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

		if (bufftime > 0) 
		{
			bufftime -= Time.deltaTime;
		}

		else 
		{
			if(tempbuffstr)
				GameObject.FindGameObjectWithTag ("PlayerData").GetComponent<PlayerData> ().playerStr -= 3;
			if(tempbuffvit)
				GameObject.FindGameObjectWithTag ("PlayerData").GetComponent<PlayerData> ().playerVit -= 3;
			if(tempbuffspr)
				GameObject.FindGameObjectWithTag ("PlayerData").GetComponent<PlayerData> ().playerSpr -= 3;
			if(tempbuffint)
				GameObject.FindGameObjectWithTag ("PlayerData").GetComponent<PlayerData> ().playerInt -= 3;


			tempbuffstr = false;
			tempbuffvit = false;
			tempbuffspr = false;
			tempbuffint = false;
		}

		for(int x = 0; x < Inventorylist.Count; x++) 
		{
			Inventorylist [x].stockcount.text = "" + Inventorylist[x].stock;
		}
	}

	public void Additem(int itemnumber)
	{
		Inventorylist [itemnumber].stock += 1;
	}

	public void description(int x)
	{
		Inventorylist [x].Descriptionbox.text = Inventorylist [x].Description;
	}

	public void available()
	{
		
	}

	public void healing(int x)
	{
		if (Inventorylist [x].stock > 0) 
		{
			Inventorylist [x].stock -= 1;
			GameObject.FindGameObjectWithTag ("PlayerData").GetComponent<PlayerData> ().playerCurrentHP += Inventorylist [x].heal;
		}
	}

	public void regen(int x)
	{
		if (Inventorylist [x].stock > 0) 
		{
			Inventorylist [x].stock -= 1;
			GameObject.FindGameObjectWithTag ("PlayerData").GetComponent<PlayerData> ().playerCurrentMP += Inventorylist [x].heal;
		}
	}

	public void buffingStr(int x)
	{
		if (Inventorylist [x].stock > 0) 
		{
			Inventorylist [x].stock -= 1;
			GameObject.FindGameObjectWithTag ("PlayerData").GetComponent<PlayerData> ().playerStr += Inventorylist [x].buff;
			tempbuffstr = true;
			bufftime = 60;
		}
	}

	public void buffingVit(int x)
	{
		if (Inventorylist [x].stock > 0) 
		{
			Inventorylist [x].stock -= 1;
			GameObject.FindGameObjectWithTag ("PlayerData").GetComponent<PlayerData> ().playerVit += Inventorylist [x].buff;
			tempbuffvit = true;
			bufftime = 60;
		}
	}

	public void buffingSpr(int x)
	{
		if (Inventorylist [x].stock > 0) 
		{
			Inventorylist [x].stock -= 1;
			GameObject.FindGameObjectWithTag ("PlayerData").GetComponent<PlayerData> ().playerSpr += Inventorylist [x].buff;
			tempbuffspr = true;
			bufftime = 60;
		}
	}

	public void buffingInt(int x)
	{
		if (Inventorylist [x].stock > 0) 
		{
			Inventorylist [x].stock -= 1;
			GameObject.FindGameObjectWithTag ("PlayerData").GetComponent<PlayerData> ().playerInt += Inventorylist [x].buff;
			tempbuffint = true;
			bufftime = 60;
		}
	}
}
