﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour {

	[SerializeField]
	GameObject[] spawnpoint;
	[SerializeField]
	GameObject[] enemytype;
	[SerializeField]
	float spawntime;
	[SerializeField]
	List<GameObject> spawned = new List<GameObject>();
	[SerializeField]
	int maxspawned;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		int randomwspawnpoint;


		spawntime += Time.deltaTime;

		if (spawntime >= 3) 
		{
			spawned.Clear ();
			randomwspawnpoint = Random.Range (0, spawnpoint.Length);
			spawntime = 0;
			if (GameObject.FindGameObjectsWithTag ("Enemy").Length < 3) 
			{
				GameObject tempspawn = Instantiate (enemytype [Random.Range (0, enemytype.Length)], spawnpoint [randomwspawnpoint].transform.position, Quaternion.identity) as GameObject;
			}

			foreach(GameObject spawn in GameObject.FindGameObjectsWithTag("Enemy"))
			{
				spawned.Add (spawn);
			}
		}
			
	}
}
