﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Detector : MonoBehaviour {

    //public GameObject EnemyState;

	float dist;

    public int attackRange;

    private EnemyState StateCheck;

	private GameObject Player;

	// Use this for initialization
	void Start () 
    {
        StateCheck = this.transform.parent.GetComponent<EnemyState>();
        Player = GameObject.FindGameObjectWithTag("Player").gameObject;

	}
	
	// Update is called once per frame
	void Update () 
    {
         dist = Vector3.Distance(Player.transform.position, this.transform.position);

       // Debug.Log("Distance : " + dist);

        if (dist < attackRange + 1)
        {
            this.StateCheck.ToAttackState(Player.gameObject);
        }
}



    void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.CompareTag("Player"))
        {
            this.StateCheck.ToChaseState(col.gameObject);
        }
    }

	void OnTriggerStay(Collider col)
	{
		if (col.gameObject.CompareTag("Player") && dist > attackRange + 1)
		{
			this.StateCheck.ToChaseState(col.gameObject);
		}
	}

    void OnTriggerExit(Collider col)
    {
        if (col.gameObject.CompareTag("Player"))
        {
            this.StateCheck.ToPatrolState(col.gameObject);
        }
    }
}
