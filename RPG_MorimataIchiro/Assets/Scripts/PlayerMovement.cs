﻿using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections;
using UnityEngine.UI;
public class PlayerMovement : MonoBehaviour {

	UnityEngine.AI.NavMeshAgent	kokoAgent;

	public GameObject		markerPoint;
	public Animator 		kokoAnimation;

	public bool 			wayPoint;

	//public Text strenght;

	//int money;
	// Use this for initialization
	void Start () 
	{
		kokoAgent 		= GetComponent<UnityEngine.AI.NavMeshAgent> ();
		kokoAnimation 	= GetComponent<Animator> ();

		//kokoAnimation.SetInteger ("KoKo Index", 1);
		kokoAnimation.SetTrigger("KK_Idle");
	}
	
	// Update is called once per frame
	void Update () 
	{
		if (EventSystem.current.IsPointerOverGameObject ())
			return;
		//money += 5;
		//strenght.text = "Str: " + money.ToString ();
		if (Input.GetMouseButtonDown(0)) 
		{
			RaycastHit hit;


			if (Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hit, 100)) 
			{
				kokoAgent.destination = hit.point;

				//kokoAnimation.SetInteger ("KoKo Index", 2);
				kokoAnimation.SetTrigger("KK_Run_No");


				if (wayPoint == false) 
				{
					Instantiate (markerPoint, hit.point, Quaternion.identity);
				}

				wayPoint = true;
			}
		}
			
	}

	void OnTriggerEnter(Collider KokoCol)
	{
		kokoAnimation.SetTrigger("KK_Idle");
		wayPoint = false;

		if (KokoCol.gameObject.CompareTag("Marker"))
		{
			Destroy (KokoCol.gameObject);
		}

	}
}
